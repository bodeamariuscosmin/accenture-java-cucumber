@test @title
Feature: BMC Homepage Tests

  Scenario: BMC Homepage Title Test
    Given I navigate to "homepage"
    Then the title of the page should be "BMC - Homepage"