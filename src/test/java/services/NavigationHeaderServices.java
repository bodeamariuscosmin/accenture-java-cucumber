package services;

import cucumber.api.PendingException;
import cucumber.pages.NavigationHeaderPage;
import cucumber.utils.Helpers;
import framework.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class NavigationHeaderServices extends BaseClass {

    public NavigationHeaderServices() { }

    public NavigationHeaderServices(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void iClickTheButtonFromHeader(String button) {
        NavigationHeaderPage navigationHeaderPage = new NavigationHeaderPage(driver);
        WebElement elementToClick;

        switch(button.toLowerCase())
        {
            case "log in":
                elementToClick = navigationHeaderPage.logInButton;
                break;
            default:
                throw new PendingException("Unknown button: " + button);
        }

        Helpers.clicker(elementToClick);
    }
}
