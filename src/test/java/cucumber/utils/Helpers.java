package cucumber.utils;

import cucumber.api.Scenario;
import framework.BaseClass;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Helpers extends BaseClass {

    public static void takeScreenShot(Scenario currentScenario){
        try {
            byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES); //For local drivers only.
            currentScenario.embed(screenshot, "image/png");
        } catch (WebDriverException wde) {
            System.err.println(wde.getMessage());
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }
    }

    public static void clicker(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public static void waiter(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void waiterForList(List<WebElement> element){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfAllElements(element));
    }

    public static void notToBeDisplayed(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.presenceOfElementLocated((By) element));
    }

    public static void hoverOverElement(WebElement hoverOverElement, WebElement element) {
        Actions builder = new Actions(driver);
        waiter(hoverOverElement);
        builder.moveToElement(hoverOverElement).perform();
        waiter(element);
    }
}
