package cucumber.steps;

import cucumber.api.java.en.When;
import framework.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import services.NavigationHeaderServices;

public class NavigationHeaderSteps extends BaseClass {

    public NavigationHeaderSteps() { }

    public NavigationHeaderSteps(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @When("I click the \"(.*)\" button from header")
    public static void iClickTheButtonFromHeader(String button) throws InterruptedException {
        NavigationHeaderServices navigationHeaderServices = new NavigationHeaderServices(driver);

        navigationHeaderServices.iClickTheButtonFromHeader(button);
    }
}
